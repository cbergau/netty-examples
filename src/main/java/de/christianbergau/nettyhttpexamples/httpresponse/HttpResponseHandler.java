package de.christianbergau.nettyhttpexamples.httpresponse;

import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;

public class HttpResponseHandler extends SimpleChannelInboundHandler<DefaultHttpMessage> {
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, DefaultHttpMessage httpMessage) throws Exception {
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer("Test".getBytes())
        );

        channelHandlerContext.writeAndFlush(response).addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture channelFuture) {
                if (channelFuture.isSuccess()) {
                    channelFuture.channel().close();
                } else {
                    System.out.println(channelFuture.cause().getMessage());
                    channelFuture.channel().close();
                }
            }
        });
    }
}
